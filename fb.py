# import sys
# import arithmetic as arithmetic
from tqdm import tqdm
for i in tqdm(range(10000)):
    pass

# print(arithmetic.plus(1, 2))

# def BB():
#     import arithmetic as arithmetic

# BB()
# print(dir(sys))
# print(arithmetic.__package__)

# """This is a module for our Person class.
# .. moduleauthor: Jane Smith <jane.smith@example.com>
# """

# import datetime

# class Person:
#     """This is a class which represents a person. It is a bit of a silly class.
#     It stores some personal information, and can calculate a person's age.
#     """

#     def __init__(self, name, surname, birthdate, address, telephone, email):
#         """This method creates a new person.

#         :param name: first name
#         :type name: str
#         :param surname: surname
#         :type surname: str
#         :param birthdate: date of birth
#         :type birthdate: datetime.date
#         :param address: physical address
#         :type address: str
#         :param telephone: telephone number
#         :type telephone: str
#         :param email: email address
#         :type email: str
#         """

#         self.name = name
#         self.surname = surname
#         self.birthdate = birthdate

#         self.address = address
#         self.telephone = telephone
#         self.email = email

#     @classmethod.mro
#     def age(self):
#         """This method calculates the person's age from the birthdate and the current date.

#         :returns: int -- the person's age in years
#         """
#         today = datetime.date.today()
#         age = today.year - self.birthdate.year

#         if today < datetime.date(today.year, self.birthdate.month, self.birthdate.day):
#             age -= 1

#         return age