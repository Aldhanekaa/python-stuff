def multiple(x = 2, y = None)->int:
    if (not y):
        return lambda y: x * y
    
    return x * y

print(multiple(3)(2))

def f(pos1, pos2, /, pos_or_kwd, *, kwd1, kwd2):

    print('hey')

