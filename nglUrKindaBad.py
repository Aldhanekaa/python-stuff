import random, os, time
from sys import platform
import platform

def KeyboardInterruptEvent(input):
    if input == 'Y':
        for i in range(10):
            f = ''
            for i2 in range(i):
                f = f+'.'
            print(f'Stopping{f}')
            time.sleep(0.1)
        return True
    if input == 'N':
        return False
        pass
    else:
        print('I SAID Y/N !!!')
        os.system('pause')
        pass

def clearConsole():

    OS = platform.system()
    print(OS)

    if OS == "Linux" or OS == "Darwin":
        clear = lambda: os.system('clear') #on Linux System
    else:
        clear = lambda: os.system('cls')

    clear()



clearConsole()

welcome = """================================================================
  _   _   _  _____   _      ____    _____     __________  _____
 | | | | | | |  __| | |    /  __|  |  _  |   |  _    _  | |  __|
 \ \_| |_/ / |  __| | |__  | |__   | |_| |   | | |  | | | |  __|
  \_______/  |____| |____|  \___|  \_____/   |_| |__| |_| |____|
================================================================
"""
print(welcome)
os.system('pause')

yesAndNo = ('Y', 'N', 'y', 'n')
yes = ('Y', 'y')
no = ('N', 'n')

while True:
    abcd = input('Do you want a tutorial? (Y/N) ').upper()
    if abcd not in yesAndNo:
        print('CHOOSE BETWEEN Y/N !')
    else:
        break

startGame = False
if abcd == 'Y':
    clearConsole()
    print('Questions will appear in the console.')
    os.system('pause')
    print('You need to write the answer as a number, not a letter')
    os.system('pause')
    if platform == 'win32':
        print('To stop program, use CTRL + C')
    elif platform == 'darwin':
        print('To stop program, use CMD + C')
    else:
        print('To stop program, use CTRL + C or CMD + C')
    os.system('pause')
    print('Happy Counting! \U0001F60A')


    errorinputForStartGame = False

    while True:
        if errorinputForStartGame:
            print('CHOOSE BETWEEN Y/N !')
        inputForStartGame = input('Do you want to start the game? (Y/N) ').upper()

        if inputForStartGame not in yesAndNo:
            errorinputForStartGame = True
            continue
        else:
            if inputForStartGame in yes:
                break
            else:
                errorinputForStartGame = False
                continue
else:
    pass

def ask():
    a = random.randint(1,100)
    b = random.randint(1,100)
    c = 0
    notNum = False

    while True:
        try:
            clearConsole()

            if (notNum):
                print('TYPE A NUMBER!!')

            c = int(input(f'{a} + {b} = '))
            break
        except ValueError:
            notNum = True
            continue

    d = int(a)+int(b)
    if c == d:
        print('Nice one')
        os.system('pause')

    elif c != d:
        print('Ur kinda bad ngl')
        os.system('pause')

while True:
    try:
        ask()
    except KeyboardInterrupt:
        print('\nKeyboardInterrupt Received')
        e = input('Do you want to stop? (Y/N) ').upper()

        x = KeyboardInterruptEvent(e)

        if (not x):
            continue
        elif (x):
            break
