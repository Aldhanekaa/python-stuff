words_file = open('words.txt', 'r')

words_count = dict()
word_counts = list()

for line in words_file:
    line = line.strip()
    line = line.split()
    for word in line:
        # print(word.lower())
        # print(words_count.get(word.lower(), 0) + 1)
        words_count[word.lower()] = words_count.get(word.lower(), 0) + 1

word_counts = [v for (k,v) in words_count.items()]
word_counts = sum(word_counts)
word_keys =  words_count.keys()

print(f"there are {word_counts} words")

thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdictCC = thisdict.copy()
thisdictCC.popitem()  # removes the last inserted item 
thisdictCC.pop("model")
if "brand" in thisdict:
    del thisdict["brand"]

print(thisdict)


car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
del car['model']
car.setdefault("model", "Bronco")
print(dir(car))
# car.update({"hey": 2})
try:
    del car['hey']
except:
    print("helo")
print(car)
print("*car ",*car)
print( len(car.values()))
