import smtplib, ssl

port = 587  # For starttls
smtp_server = "smtp.gmail.com"
sender_email = "[secret]"
receiver_email = "[secret]"
password = input("Type your password and press enter:")
message = """
Hello Aldhan, here is your Auth Token: 239845234234
"""

context = ssl.create_default_context()
with smtplib.SMTP(smtp_server, port) as server:
    server.ehlo()  # Can be omitted
    server.starttls()
    server.ehlo()

    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, message)


print("OK!")