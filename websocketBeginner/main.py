import socket


conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect(('data.pr4e.org', 80))
cmd = "GET http://data.pr4e.org/romeo.txt HTTP/1.0\r\n\r\n".encode() # this type is bytes
# print(type(cmd))
conn.send(cmd)

print("Sent!")

while True:
    data = conn.recv(512)
    if len(data) < 1:
        break
    print("got it!")
    print(data.decode())

print("done")

conn.close()

print(ord("w"))