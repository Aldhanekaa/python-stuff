import os
import platform

menu = "main"
_file = None

def start_CLI():
    global menu
    global _file

    while (menu):
        if (menu == "main"):
            clearConsole()
            start_CLI_menu()
        elif (menu == "x"):
            clearConsole()
            menu = False
        elif (menu == "c" and _file == None):
            clearConsole()
            start_CREATE_menu()
        if (menu == "q"):
            menu = "main"
            continue

        if (menu):
            if (menu == "c"):
                create_file_input = str(input("> "))
                if (create_file_input != "q" and create_file_input != "x"):
                    clearConsole()
                    file_content = input("""
File Content Please
> """)
                    create_file(create_file_input)
                    
                    _file.write(file_content)

                else:
                    menu = create_file_input
                
            else:
                menu = str(input("> "))



def start_CLI_menu():
    print("""
=========== Welcome Back Aldhaneka ===========

    * press c to create file
    * press d to delete file
    * press e to edit file
    * press x to force quit

==============================================
    """)

def start_CREATE_menu():
    print("""
=========== Welcome Back Aldhaneka ===========

    * what file name would you like to create Aldhaneka?
    * press q to menu
    * press x to force quit

==============================================
    """)

def create_file(file_name):
    global _file

    _file = open(file_name, "w")
    return

def write_file(file_name, file_content):
    global _file

    _file = open(file_name, "w")
    _file.write(file_content)

    return


# def createTable
def clearConsole():

    clear = lambda: os.system('clear') #on Linux System

    clear()



start_CLI()